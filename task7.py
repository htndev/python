try:
    a = int(input('Первое значение: '))
    b = int(input('Второе значение: '))
    c = int(input('Третье значение: '))
    arr = [a, b, c]
    print('{}\n{}\n{}'.format(max(arr), min(arr), sorted(arr)[1]))

except ValueError:
    print('Введите корректные значения!')