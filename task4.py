try:
    year = int(input("Введите год:  "))
    if 1900 <= year <= 3000:
        if (year % 4) == 0:
            if (year % 100) == 0:
                if (year % 400) == 0:
                    print("Высокосный!")
                else:
                    print("Не высокосный!")
            else:
                print("Высокосный!")
        else:
            print("Не высокосный!")
    else:
        print('Год должен быть не меньше 1900 и не больше 3000!')
except ValueError:
    print("Введите число!")
