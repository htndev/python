try:
    inp = int(input('Введите число (-15, 12] U (14, 17) U [19, +Infinity): '))
    if -15 < inp <= 12 or 14 < inp < 17 or 19 <= inp:
        print(True)
    else:
        print(False)
except ValueError:
    print('Введите число!')